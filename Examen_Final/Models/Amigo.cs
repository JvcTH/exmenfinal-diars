﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_Final.Models
{
    public class Amigo
    {
        public int Id { get; set; }
        public int AmigoId { get; set; }
        public int UsuarioId { get; set; }
        public bool Solicitud { get; set; }
        public Usuario Usuario { get; set; }
    }
}
