﻿using Examen_Final.DB;
using Examen_Final.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_Final.Controllers
{


    public class CuentaController : Controller
    {
        private AppFinalContext context;
        public CuentaController(AppFinalContext context)
        {
            this.context = context;
        }

        // GET: CuentaController
        public ActionResult Index()
        {
            double credito = 0;
            double propio = 0;
            double deuda = 0;
            var usuario = GetUsuario();
            var cuentas = context.Cuentas.Where(o => o.UsuarioId == usuario.Id).ToList();
            if (cuentas.Count > 0) {
                foreach (var item in cuentas) {
                    credito = credito + item.SaldoInicial;
                    if (item.Categoria == "Propia")
                    {
                        propio = propio + item.SaldoInicial;
                    }
                    else {
                        var gastos = context.Gastos.Where(o => o.CuentaId == item.Id).ToList();
                        var ingresos = context.Ingresos.Where(o => o.CuentaId == item.Id).ToList();
                        foreach (var ingreso in ingresos)
                        {
                            deuda = deuda + ingreso.Monto;
                        }
                        foreach (var gasto in gastos) {
                            deuda = deuda - gasto.Monto;
                        }
                    }
                }
            }

            if (deuda<0) {
                propio = propio + deuda;
            }


            ViewBag.Deuda= deuda;
            ViewBag.Propio = propio;
            ViewBag.Credito = credito;
            return View(cuentas);
        }

        [HttpGet]
        public ActionResult Crear()
        {
            return View();
        }
        [HttpPost]
        public ActionResult GuardarCuenta(Cuenta cuenta)
        {
            var usuario = GetUsuario();
            cuenta.UsuarioId = usuario.Id;
            context.Cuentas.Add(cuenta);
            context.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult IndexIngresos(int id)
        {
            var cuenta = context.Cuentas.Where(o => o.Id == id).FirstOrDefault();
            var ingresos = context.Ingresos.Where(o => o.CuentaId == cuenta.Id);
            ViewBag.Cuenta = cuenta;
            return View(ingresos);
        }

        public ActionResult IndexGastos(int id)
        {

            var cuenta = context.Cuentas.Where(o => o.Id == id).FirstOrDefault();
            var gastos = context.Gastos.Where(o => o.CuentaId == cuenta.Id);
            ViewBag.Cuenta = cuenta;
            return View(gastos);
        }

        [HttpGet]
        public ActionResult Gastar()
        {
            var usuario = GetUsuario();
            ViewBag.cuentas = context.Cuentas.Where(o => o.UsuarioId == usuario.Id).ToList();

            return View(new Gasto());
        }
        [HttpPost]
        public ActionResult Gastar(Gasto gasto)
        {
            var cuenta = context.Cuentas.Find(gasto.CuentaId);

            if (gasto.Monto <= cuenta.SaldoInicial) {
                gasto.CuentaDestino = "Gasto Propio";
                
                cuenta.SaldoInicial = cuenta.SaldoInicial - gasto.Monto;
                context.Gastos.Add(gasto);
                context.SaveChanges();

                return RedirectToAction("IndexGastos", new { id = gasto.CuentaId });
            }
            var usuario = GetUsuario();
            ViewBag.cuentas = context.Cuentas.Where(o => o.UsuarioId == usuario.Id).ToList();
            ModelState.AddModelError("Monto", "El monto ingresado es mayor al monto de la cuenta");
            return View(gasto);

        }

        [HttpGet]
        public ActionResult Ingresar()
        {
            var usuario = GetUsuario();
            ViewBag.cuentas = context.Cuentas.Where(o => o.UsuarioId == usuario.Id).ToList();
            
            return View();
        }

        [HttpPost]
        public ActionResult Ingresar(Ingreso ingreso)
        {
            ingreso.CuentaRemitente = "Ingreso Propio";
            var cuenta = context.Cuentas.Find(ingreso.CuentaId);

            cuenta.SaldoInicial = cuenta.SaldoInicial + ingreso.Monto;
            context.Ingresos.Add(ingreso);
            context.SaveChanges();
            return RedirectToAction("IndexIngresos",new {id = ingreso.CuentaId });
        }
        [HttpGet]
        public ActionResult TransferenciaCuentas()
        {
            var usuario = GetUsuario();
            ViewBag.cuentas = context.Cuentas.Where(o => o.UsuarioId == usuario.Id).ToList();

            return View(0);
        }

        [HttpPost]
        public ActionResult TransferenciaCuentas(float monto, int idCuenta1, int idCuenta2, DateTime fecha)
        {
            var cuentaOrigen = context.Cuentas.Find(idCuenta1);
            if (monto <= cuentaOrigen.SaldoInicial) {
                var cuentaDestino = context.Cuentas.Find(idCuenta2);
                
                var gasto = new Gasto() { CuentaId = idCuenta1, 
                    CuentaDestino = cuentaDestino.Nombre, 
                    Fecha = fecha,
                    Monto = monto,
                    Descripcion = "Transferencia" };
                var ingreso = new Ingreso() { CuentaId = idCuenta2, 
                    CuentaRemitente = cuentaOrigen.Nombre, 
                    Fecha=fecha,
                    Monto = monto,
                    Descripcion = "Transferencia" };
                cuentaOrigen.SaldoInicial = cuentaOrigen.SaldoInicial - monto;
                cuentaDestino.SaldoInicial = cuentaDestino.SaldoInicial + monto;
                context.Gastos.Add(gasto);
                context.Ingresos.Add(ingreso);
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            var usuario = GetUsuario();
            ViewBag.cuentas = context.Cuentas.Where(o => o.UsuarioId == usuario.Id).ToList();

            ModelState.AddModelError("Monto", "El monto ingresado es mayor al monto de la cuenta" + cuentaOrigen.SaldoInicial);
            return View(monto);
        }
        [HttpGet]
        public ActionResult TransferenciaAmigo(int id)
        {
            var usuario = GetUsuario();
            ViewBag.cuentas = context.Cuentas.Where(o => o.UsuarioId == usuario.Id).ToList();
            ViewBag.cuentasAmigo = context.Cuentas.Where(o => o.UsuarioId == id).ToList();

            return View(0);
        }
        [HttpPost]
        public ActionResult TransferenciaAmigo(float monto, int idMiCuenta, int idCuentaAmigo, DateTime fecha)
        {
            var cuentaOrigen = context.Cuentas.Find(idMiCuenta);
            if (monto <= cuentaOrigen.SaldoInicial)
            {
                var cuentaDestino = context.Cuentas.Find(idCuentaAmigo);

                var gasto = new Gasto()
                {
                    CuentaId = idMiCuenta,
                    CuentaDestino = cuentaDestino.Nombre,
                    Fecha = fecha,
                    Monto = monto,
                    Descripcion = "Transferencia a amigo"
                };
                var ingreso = new Ingreso()
                {
                    CuentaId = idCuentaAmigo,
                    CuentaRemitente = cuentaOrigen.Nombre,
                    Fecha = fecha,
                    Monto = monto,
                    Descripcion = "Transferencia a amigo"
                };
                cuentaOrigen.SaldoInicial = cuentaOrigen.SaldoInicial - monto;
                cuentaDestino.SaldoInicial = cuentaDestino.SaldoInicial + monto;
                context.Gastos.Add(gasto);
                context.Ingresos.Add(ingreso);
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            var usuario = GetUsuario();
            ViewBag.cuentas = context.Cuentas.Where(o => o.UsuarioId == usuario.Id).ToList();

            ModelState.AddModelError("Monto", "El monto ingresado es mayor al monto de la cuenta" + cuentaOrigen.SaldoInicial);
            return View(monto);
        }
        public ActionResult IndexUsuarios()
        {
            var usuarios = context.Usuarios.ToList();
            ViewBag.Usuario = GetUsuario();
            return View(usuarios);
        }
        public ActionResult IndexAmigos()
        {
            var usuario = GetUsuario(); 
            var solicitudes = context.Amigos.Where(o=>o.AmigoId == usuario.Id && o.Solicitud==false).ToList();
            var amigos1 = context.Amigos.Where(o => o.AmigoId == usuario.Id && o.Solicitud == true).ToList();
            var amigos2 = context.Amigos.Where(o => o.UsuarioId == usuario.Id && o.Solicitud == true).ToList();

            var usuariosSolicitudes = new List<Usuario>();
            var ususariosAmigos = new List<Usuario>();
            var usuarios = context.Usuarios.ToList();

            foreach (var item in solicitudes) {
                usuariosSolicitudes.Add(context.Usuarios.Find(item.UsuarioId));
            }
            foreach (var item in amigos1)
            {
                ususariosAmigos.Add(context.Usuarios.Find(item.UsuarioId));
            }
            foreach (var item in amigos2)
            {
                ususariosAmigos.Add(context.Usuarios.Find(item.AmigoId));
            }
            ViewBag.Solicitudes = usuariosSolicitudes;
            ViewBag.Amigos = ususariosAmigos;
            return View();
        }

        public ActionResult EnviarSolicitud(int id)
        {
            var amigo = context.Usuarios.Where(o=>o.Id==id).FirstOrDefault();
            var usuario = GetUsuario();
            
            var amistad = new Amigo() {UsuarioId = usuario.Id,
                AmigoId = amigo.Id,
                Solicitud = false
            };
            context.Amigos.Add(amistad);
            context.SaveChanges();
            ViewBag.Usuario = GetUsuario();
            return RedirectToAction("Index");
        }
        public ActionResult AceptarSolicitud(int id)
        {
            var usuario = GetUsuario();
            var solicitud = context.Amigos.Where(o => o.AmigoId == usuario.Id && o.UsuarioId == id).FirstOrDefault();
            solicitud.Solicitud = true;
            context.SaveChanges();
            return RedirectToAction("IndexAmigos");
        }
        private Usuario GetUsuario()
        {
            var claim = HttpContext.User.Claims.FirstOrDefault();
            if (claim != null)
            {
                var user = context.Usuarios.Where(o => o.Email == claim.Value).FirstOrDefault();
                return user;
            }

            return null;
        }

    }
}
