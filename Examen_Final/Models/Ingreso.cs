﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_Final.Models
{
    public class Ingreso
    {
        public int Id{ get; set; }
        public int CuentaId { get; set; }
        public string CuentaRemitente { get; set; }

        public DateTime Fecha { get; set; }
        public double Monto{ get; set; }
        public string  Descripcion { get; set; }

    }
}
